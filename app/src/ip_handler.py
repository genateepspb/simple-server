import http.server
import os
import psycopg2
import socketserver


class IPHandler(http.server.SimpleHTTPRequestHandler):
    def handle_one_request(self):
        try:
            conn = psycopg2.connect(user="ip-handler",
                                    password=os.getenv('password'),
                                    host="postgres-svc",
                                    port="5432",
                                    database="ip_db")
            cursor = conn.cursor()
            cursor.execute("INSERT INTO client_ips (ip) VALUES (%s);", (self.client_address[0],))
            conn.commit()
            print("Record inserted successfully into client_ips table")
        except (Exception, psycopg2.Error) as error:
            print(error)
        finally:
            if conn:
                cursor.close()
                conn.close()
                print("PostgreSQL connection is closed")
        return super().handle_one_request()


httpd = socketserver.TCPServer(("", 8008), IPHandler)

while True:
    httpd.handle_request()
